ENV['APP_ENV'] = 'test'

setup_folder = File.join("../../",__dir__,"setup")

$LOAD_PATH.unshift(setup_folder)

require 'site_key'
require 'minitest/autorun'
require 'rack/test'

class SetupTest <  Minitest::Test
  include Rack::Test::Methods

  def app
    Sinatra::Application
  end

  def test_it_creates_a_key
    get '/'
    assert last_response.ok?
    assert_equal 'Hello World', last_response.body
  end

  def test_it_can_create_env_file
    File.new()
    
  end

  def test_it_can_set_permissions
  end
  
end