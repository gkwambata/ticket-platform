ENV['APP_ENV'] = 'test'

require_relative '../../app'
require 'capybara'
require 'capybara/dsl'
require 'minitest/autorun'


class PageTest <  Minitest::Test

  include Capybara::DSL
  # Capybara.default_driver = :selenium # <-- use Selenium driver

  def setup
    Capybara.app = Sinatra::Application.new
  end

  def test_it_works
    visit '/'
    assert page.has_content?('Hello World')
  end

  def test_it_has_homepage
    visit   '/'
    assert last_response.ok?
  end

  def test_it_has_product_page
    get '/product', :product_id => 123
    assert last_response.ok?
  end
 
  def test_it_has_cart_page
    get '/cart',:cart_id=123
    assert last_response.ok?
  end

  def test_it_has_checkout_page
    get '/checkout'
    assert last_response.ok?
  end

  def test_it_has_terms_page
    get '/terms'
    assert last_response.ok?
  end
  
   def test_it_has_privacy_page
    get '/privacy'
    assert last_response.ok?
  end

  def test_it_has_faq_page
    get '/faq'
    assert last_response.ok?
  end

  def test_it_has_contact_page
    get '/contact'
    assert last_response.ok?
  end
  
end