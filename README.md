# Ticket Platform

This is an implementation of ruby's sinatra library. I intend to build a website that has e-commerce capabilities.
The site will offer visitors a chance to win motor bikes /vehicles or receive a cashback equivalent. The site will facilitate purchasing of tickets and/or select a winner weekly.

I chose sinatra because the tasks described don't need a framework ecosystem. Atleast for this draft

It will show not only show my ruby skill level but also how i approach building an e-commerce platform on docker and the considerations it would entail.
I also use such repos to experiment/test out ideas such as 
* Engineering patterns (Solid, TDD, Clean Architecture )
- Docker containerization & docker swarms
- Ruby meta programming (in development on the setup folder)
* Heroku integration (possibly using terraform)

## How it works

On the first run of the application, the setup process will 
1. Create the .env file
2. Create APP key to secure sessions
3. Run database migrations (Coming soon)

Once completed, the site will be active.

This site currently uses MVC architecture(* will soon be replaced by clean architecture)


Assuming you have already navigated to the app on the terminal

## Setup( Docker)

Configure the bundler by running docker run --rm -v "$PWD":/usr/src/app -w /usr/src/app ruby:latest bundle config set --local with development
To create the Gemfile.lock  to run  docker run --rm -v "$PWD":/usr/src/app -w /usr/src/app ruby:<version>  bundle install 
Run the build command docker build -t <app/image name> .
Finally run docker run --rm -d -p <port>:<port> -- name <container name> <image name>
Go to the browser and type localhost:<port>

## Setup (local machine)

Configure the bundler by running  bundle config set --local with development
Create the Gemfile.lock by running bundle install 
Finally run ruby app.rb -p <port>
Go to the browser and type localhost:<port>



## Troubleshooting

This is an implementation of the sinatra library. Incase any of any issue you you may google it and attach "sinatra" to your search