# /usr/bin/env ruby

#Load Configurations

#Start Application
require 'sinatra'
require "sinatra/reloader" if development?


require_relative './config'
require_relative 'routes/web'
