# Purpose

Setup the website defaults and ensure it is secure 

## Current setup
1. Create .env file
2. Application secret. Generate it using a rsecurerandom
3. Logging

# Activation

I intend to make calling this section automatically on the first run of the application. This will ensure the application runs as intended.
