#if $env['logging_enabled']
set :logging, true
set :dump_errors, true if settings.environment == "development"
#end