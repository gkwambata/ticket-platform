
raise LoadError , "Configuration issue detected" if $env['APP_ENV'] == nil

case $env['APP_ENV'].downcase

when "production"
	set :environment , :production
when "development"
	set :environment, :development
else
	set :environment, :development
end


#set :root, File.expand_path("..", Dir.pwd)
set :root, File.expand_path( Dir.pwd )


set :public_folder, $env['PUBLIC_FOLDER'] if $env['PUBLIC_FOLDER']

set :port, $env['APP_PORT'] if $env['APP_PORT']

set :bind, '0.0.0.0'