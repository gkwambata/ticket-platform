FROM ruby:latest

MAINTAINER George Gathura <george@teamgigo.com>
# throw errors if Gemfile has been modified since Gemfile.lock
RUN bundle config --global frozen 1

WORKDIR /usr/src/app

COPY Gemfile Gemfile.lock ./
ENV GEM_HOME="/usr/local/bundle"
ENV PATH $GEM_HOME/bin:$GEM_HOME/gems/bin:$PATH

RUN bundle install

COPY . .

EXPOSE 4567

ENTRYPOINT [ "./boyb.sh" ]
#CMD ["ruby", "app.rb"]