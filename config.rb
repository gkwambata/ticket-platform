begin
	
	settings = File.new( File.join(__dir__,".env") )
	$env={}
	settings.each do |setting|

		if setting.split('=').length == 2 #i.e config = "its value"
			key ,value = setting.split('=')	
			#puts "Loading Config #{key} with #{value}"
			$env[key.to_s] = value
		end

	end
	settings.close

rescue SystemCallError
	puts "Configuration Issue detected. Check configuration files .env and #{__FILE__}"	
	exit
end

raise LoadError "Configuration issues detected. 
		.env file is empty. Either run 'rake setup' to configure application 
		or make a copy of .env.example" if $env.length == 0


require_relative 'config/environment'
require_relative 'config/headers'
require_relative 'config/logs'
require_relative 'config/sessions'

