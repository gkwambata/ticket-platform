#homepage
get '/' do
  session[:welcome] = "Welcome to Ticket Platform"
  #cookie[:accept_privacy_policy]=true
  erb :index
end



#Product page
get '/product/:product_id' do | product_id |
  'do this do that to product #{product_id}'
end



#Cart Page
get '/cart/:cart_id' do |cart_id|
  'do this do that'
end



#Checkout Page
get '/checkout/:cart_id' do |cart_id|
  'do this do that'
end



#FAQ
get '/faq' do
  'do this do that'
end



#terms of use
get '/terms' do
  'do this do that'
end



#privacy policy
get '/privacy' do
  'do this do that'
end



#Contact Us Page
get '/contact' do
  'do this do that'
end

not_found do
  redirect '404.html'
end